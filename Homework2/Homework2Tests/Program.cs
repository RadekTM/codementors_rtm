﻿using System;
using ConsoleTools.Menu;
using System.Collections.Generic;
using System.Linq;
using Homework2Tests.BL;
using Homework2Tests.BL.Model;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json;

namespace Homework2Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRepository testRepository = new TestRepository();
            var exitOption = new ExitOption();
            
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                new GeneralOption("Dodanie nowej kategorii",testRepository.AddCathegory),
                new GeneralOption("Spis kategorii",testRepository.CathegoryList),
                new GeneralOption("Dodaj pytanie do bazy testów",testRepository.AddTestQuestion),
                new GeneralOption("Przetestuj swoją wiedzę",testRepository.Test),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);
            Console.ReadLine();
        }

    }
}
