﻿using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace ConsoleTools.Menu
{
    public class ConsoleMenu
    {
        private readonly IMenuOption[] _options;


        //to słuzy do podania opcji do menu i następnie układa je alfabetycznie w tavlicy _options
        public ConsoleMenu(IMenuOption[] options)
        {
            //_options = options.OrderBy(o=>o.Description).ToArray();
            _options = options.ToArray();
        }

        //to bierze tablice _options, wyświetlna na ekranie menu wypełnione opcja mi zdefiniowanymi
        //w funkci console menu
        public void Show()
        {
            WriteLine("Wybierz opcję");
            int i = 1;
            foreach (var option in _options)
            {
                WriteLine($"{i++} {option.Description}");
            }
            //tutaj czeka na nasz wybór i sprawdza czy vo wybraliśmy pokrywa się z ilością dostęnych opcji
            //do tego służy _options.Length
            uint choice;
            while (!uint.TryParse(ReadLine(), out choice)
                || choice == 0 || choice > _options.Length)
            {
                WriteLine("Nieprawidłowa opcja, wybierz jeszcze raz...");
            }
            //to wywołuje przypisanej danej opcji w menu akcje
            _options[choice - 1].Execute();
        }
    }
}
