﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2Tests.BL.Model
{
    public class Result
    {
        public int CorrectAnswers { get; set; }
        public int AllQuestions { get; set; }
        public DateTime Time { get; set; }
        public string CathegoryName { get; set; }


        public override string ToString()
        {
            return $"Data{DateTime.Now}\nWynik: {CorrectAnswers}/{AllQuestions} ({CountProcentResult(CorrectAnswers, AllQuestions)}%)";
        }

        public double CountProcentResult(int goodAnsw, int allQuest)
        {
            return Math.Round(((double)goodAnsw / allQuest) * 100);
        }

        
    }
}
