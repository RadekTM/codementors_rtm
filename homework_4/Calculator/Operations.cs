﻿using Calculator.UserCommunication;
using ConsoleApp11;
using ConsoleTools.Menu;
using Infrastructure;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Operations
    {

        IKernel kernel = new StandardKernel(new Modul());
        ConsoleOps consoleOps = new ConsoleOps();

        public void userMenu()
        {

            var exitOption = new ExitOption();
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                ///()=>{ } pusta lambda
                new GeneralOption("Dodawanie",Add),
                new GeneralOption("Odejmowanie",Diference),
                new GeneralOption("Mnożenie",Multiply),
                new GeneralOption("Dzielenie",Divide),
                new GeneralOption("Potęgowanie",Power),
                new GeneralOption("Operacja XOR",OXOR),
                new GeneralOption("Operacja OR",OOR),
                new GeneralOption("Operacja AND",OAND),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);

            Console.ReadLine();
        }

        void Add()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "dodawanie";
            d.OpperationName(opName);
            var returned = p.SummTwoNumbers(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void Diference()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "odejmowanie";
            d.OpperationName(opName);
            var returned = p.DiferenceTwoNumbers(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void Multiply()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "mnożenie";
            d.OpperationName(opName);
            var returned = p.MultiplyOfTwoNumbers(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void Divide()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "dzialenie";
            d.OpperationName(opName);
            var returned = p.DivideTwoNumbers(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void Power()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "potęgowanie/pierwiastkowanie";
            d.OpperationName(opName);
            var returned = p.PowerOfTwoNumbers(a.ImputPowValueX(), a.ImputPowValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void OXOR()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "operacja XOR";
            d.OpperationName(opName);
            var result = p.OperationXOR(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void OOR()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "operacja XOR";
            d.OpperationName(opName);
            var result = p.OperationOR(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }

        void OAND()
        {
            Console.Clear();
            var a = kernel.Get<IUserImputData>();
            var p = kernel.Get<ICalculator>();
            var s = kernel.Get<IResultRecorder>();
            var d = kernel.Get<IUserInfos>();
            string opName = "operacja XOR";
            d.OpperationName(opName);
            var result = p.OperationAND(a.ImputValueX(), a.ImputValueY(), opName);
            consoleOps.EndOfCalculations();
        }
    }
}
