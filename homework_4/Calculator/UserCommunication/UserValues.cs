﻿using ErrorHandler;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.UserCommunication
{
    public class UserValues : IUserValues
    {
        private readonly IErrors _error;
        public UserValues(IErrors error)
        {
            _error = error;
        }

        public int ValueX()
        {

            return _error.ErrorFormatMessage();
        }

        public int ValueY()
        {
            return _error.ErrorFormatMessage();
        }

        public double PowValueX()
        {
            return _error.ErrorDoubleFormatMessage();
        }

        public double PowValueY()
        {
            return _error.ErrorDoubleFormatMessage();
        }

    }
}
