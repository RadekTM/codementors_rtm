﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.UserCommunication
{
    public class ConsoleOps
    {
        public void EndOfCalculations()
        {
            Console.WriteLine("Naciśniej dowolny klawisz aby powrócić do głównego menu...");
            Console.ReadKey();
            Console.Clear();
        }
        
    }
}
