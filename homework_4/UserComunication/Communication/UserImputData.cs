﻿using Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Communication
{
    public class UserImputData : IUserImputData
    {
        private IUserValues _mana;

        //ten konstruktor wstrzykuje imana
        public UserImputData(IUserValues mana)
        {
            _mana = mana;
        }

        public int ImputValueX()
        {
            Console.WriteLine("Podaj wartość x");
           return _mana.ValueX();
        }

        public int ImputValueY()
        {
            Console.WriteLine("Podaj wartość y");
            return _mana.ValueY();
        }

        public double ImputPowValueX()
        {
            Console.WriteLine("Podaj wartość podstawy");
            return _mana.PowValueX();
        }

        public double ImputPowValueY()
        {
            Console.WriteLine("Podaj wartość potęgi");
            return _mana.PowValueY();
        }


    }


}
