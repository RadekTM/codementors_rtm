﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HistoryLogger
{
    public class ResultRecorder : IResultRecorder
    {
        private readonly ICalculator _calc;
        public ResultRecorder(ICalculator calc)
        {
            _calc = calc;
            _calc.OperationFinished += OperationFinished;
        }

        public void OperationFinished(object sender, MyEventArgs e)
        {
            Console.WriteLine($"Wynik działania: {e.Result}");

            using (var sw = new StreamWriter(File.Open("log.txt", FileMode.Append,
                FileAccess.Write)))
            {
                sw.WriteLine($">{DateTime.Now}< wykonano działanie: {e.OpperationName}, otrzymany wynik: {e.Result}");
            }
            _calc.OperationFinished -= OperationFinished;
        }
    }
}
