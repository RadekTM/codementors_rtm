﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IUserValues
    {
        int ValueX();
        int ValueY();
        double PowValueX();
        double PowValueY();
    }
}
