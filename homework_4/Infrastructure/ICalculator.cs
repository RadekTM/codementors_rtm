﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class MyEventArgs : EventArgs
    {
        public int Result { get; set; }
        public string OpperationName { get; set; }
    }

    public class ErrorEventArgs : EventArgs
    {
        public string OpperationName { get; set; }
    }

    public interface ICalculator
    {
        event EventHandler<MyEventArgs> OperationFinished;
        event EventHandler<ErrorEventArgs> OperationError;
        int SummTwoNumbers(int a, int b, string name);
        int DiferenceTwoNumbers(int a, int b, string name);
        int MultiplyOfTwoNumbers(int a, int b, string name);
        int DivideTwoNumbers(int a, int b, string name);
        int PowerOfTwoNumbers(double a, double b, string name);
        int OperationXOR(int a, int b, string name);
        int OperationOR(int a, int b, string name);
        int OperationAND(int a, int b, string name);
    }
}
