﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookmarks.BL.Model;
using Newtonsoft.Json;

namespace Bookmarks.BL
{
    
    public class BookmarksRepository

    {
        StringChecker stringChecker = new StringChecker();
        GoBackToMenu goBackToMenu = new GoBackToMenu();

        public void Add()
        {
            Console.Clear();
            string _Description = stringChecker.NullString("Podaj nazwę zakładki: ");
            using (var context = new BookmarksContext())
            {

                var _bookmark = new Bookmark();
                var bookmarkCheck = _bookmark;

                bookmarkCheck = context.Bookmarks.FirstOrDefault
                    (a => a.Description.Equals(_Description, StringComparison.InvariantCultureIgnoreCase));
                if (bookmarkCheck != null)
                {
                    Console.WriteLine("Zakładka o takim opisie istnieje już w bazie..");
                }
                else
                {
                    string _Weblink = stringChecker.NullString("Podaj adres strony internetowej, dla tej zakładki: ");

                    _bookmark.Description = _Description;
                    _bookmark.Weblink = _Weblink;
                    context.Bookmarks.Add(_bookmark);
                    context.SaveChanges();
                    Console.WriteLine("Zakładka została dodana.");
                }

                goBackToMenu.GoBack();   
            }
        }

        public void List()
        {
            Console.Clear();
            List<Bookmark> _bookmarkList;
            using (var context = new BookmarksContext())
            {
                int id = 1;
                _bookmarkList = context.Bookmarks.ToList();
                foreach (var item in _bookmarkList)
                {
                    Console.WriteLine(id + " " + item.Description);
                    id++;
                }
            }
            goBackToMenu.GoBack();
        }

        public void OpenLinkInBrowser()
        {
            Console.Clear();
            Bookmark _bookmark;
            using (var context = new BookmarksContext())
            {
                string _bookmarkName = stringChecker.NullString("Podaj nazwę zakładki: ");
                _bookmark = context.Bookmarks.FirstOrDefault
                    (a => a.Description.Equals(_bookmarkName, StringComparison.InvariantCultureIgnoreCase));
                if (_bookmark != null)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(_bookmark.Weblink);
                    }
                    catch
                    {
                        Console.WriteLine("coś nie tak z adresem strony w tej zakładce..");
                        Console.WriteLine($"Zapisany adres stront to: {_bookmark.Weblink}");
                    }
                }
                else
                {
                    Console.WriteLine("Zakładka o takim opisie nie istnieje w bazie");
                }
            }
            goBackToMenu.GoBack();
        }

        public void SaveJsonFile()
        {
            Console.Clear();
            Bookmark _bookmark;
            using (var context = new BookmarksContext())
            {
                string _bookmarkName = stringChecker.NullString("Podaj nazwę zakładki: ");
                _bookmark = context.Bookmarks.FirstOrDefault
                    (a => a.Description.Equals(_bookmarkName, StringComparison.InvariantCultureIgnoreCase));
                if (_bookmark != null)
                {
                    Console.WriteLine("Wybrana zakladka to: ");
                    Console.WriteLine($"ID: {_bookmark.Id}, Opis: {_bookmark.Description},\nadress: {_bookmark.Weblink}");
                    var Directory = stringChecker.NullString("Podaj ścieżkę gdzie ma zostać utworzony folder do zapisu pliku..");
                    var newFolder = stringChecker.NullString("Podaj nazwę folderu do zapisu pliku ..");
                    var pathToDir = Path.Combine(Directory, newFolder);
                    var pathToFile = Path.Combine(pathToDir, "myBookmark.json");
                    CreateDirectory(pathToDir);
                    Serialize(_bookmark, pathToFile);
                    Console.WriteLine("Informacja o zakładce została dodana do pliku myBookmark.json");
                }
                else
                {
                    Console.WriteLine("Zakładka o takim opisie nie istnieje w bazie");
                }
            }
            goBackToMenu.GoBack();
        }

        static void CreateDirectory(string pathToDir)
        {
            var directory = Directory.CreateDirectory(pathToDir);

            if (directory.Exists)
            {

                Console.WriteLine("Katalog już istniał...");

            }
            else
            {
                Console.WriteLine("Katalog został utworzony");
            }
        }

        void Serialize(Bookmark _bookmark, string PathToFile)
        {
            List<Bookmark> bookmarks;
            try
            {
                string filelocalization = File.ReadAllText(PathToFile);
                bookmarks = JsonConvert.DeserializeObject<List<Bookmark>>(filelocalization);
            }
            catch
            {
                bookmarks = new List<Bookmark>();

            }
            bookmarks.Add(_bookmark);

            string jsonFile = JsonConvert.SerializeObject(bookmarks);

            File.WriteAllText(PathToFile, jsonFile);
        }
    }
}
