﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTools.Menu;

namespace Bookmarks
{
    internal class ExitOption : IMenuOption
    {
        public string Description { get; } = "Exit";
        public bool IsExitRequested { get; private set; }

        public void Execute()
        {
            IsExitRequested = true;
        }
    }
}
