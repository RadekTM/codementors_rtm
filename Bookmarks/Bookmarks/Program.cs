﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTools.Menu;
using Bookmarks.BL.Model;
using Bookmarks.BL;

namespace Bookmarks
{
    class Program
    {
        
        static void Main(string[] args)
        {
            BookmarksRepository _bookmarks = new BookmarksRepository();
            var exitOption = new ExitOption();
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                ///()=>{ } pusta lambda
                new GeneralOption("Dodanie nowej zakładki",_bookmarks.Add),
                new GeneralOption("Lista zapisanych zakładek",_bookmarks.List),
                new GeneralOption("Eksport zakładki do pliku json",_bookmarks.SaveJsonFile),
                new GeneralOption("Otwarcie zakładki w przeglądarce",_bookmarks.OpenLinkInBrowser),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);
            Console.ReadLine();
        }

        
    }
}
