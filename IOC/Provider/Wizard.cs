﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    public class Wizard
    {

        private ISpell _spell;


        public Wizard(ISpell spell)
        {
            _spell = spell;
        }


        public void CastBasicSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
