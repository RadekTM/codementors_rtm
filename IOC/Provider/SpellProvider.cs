﻿using Ninject.Activation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    //klasa provider jest z ninijecta
    public class SpellProvider:Provider<Spell>
    {
        //providery używamy kiedy chcemy odpowiednio przygotowac obiekt
        //w naszym przypadku ustawia ono pole mana
        //za każdym razem kiedy wywołamy spell będzie już miał wpisany mana cost = 20
        protected override Spell CreateInstance(IContext context)
        {
            var spell = new Spell();
            spell.ManaCost = 20;


            return spell;
        }
    }
}
