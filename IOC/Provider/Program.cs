﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    class Program
    {
        static void Main(string[] args)
        {

            //kernel to kontener IOC w nim rejestrujemy zaleznośći
            IKernel kernel = new StandardKernel();

            //tu ustawiamy zależności z powiązaniem do providera
            kernel.Bind<ISpell>().ToProvider<SpellProvider>();

            var wizard = kernel.Get<Wizard>();

            wizard.CastBasicSpell();
        }

    }
}
