﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            //kernel to kontener IOC w nim rejestrujemy zaleznośći
            //tu tworzymu instancje kontenera (nazwa kernel)
            //który trzyma zalezności
            IKernel kernel = new StandardKernel();


            //tu rejestrujemy zależność - interfejss na klasę
            //za kazdym wywołaniem interfjesu wstrzykniemy instancje klasy
            kernel.Bind<ISpell>().To<Spell>();

            //prosimy o tworzenie instancji klasy wizard
            //i przypisujemy do obiektu
            var wizard = kernel.Get<Wizard>();
            // po tej linijcie idzie do klasy wizar
            //wchodzi do klasy i widzi w niej ISpell




            //wywołujemy metode na obiekcie
            wizard.CastBasicSpell();
        }
    }
}
