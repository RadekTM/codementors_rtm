﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyInjection
{
    public class Spell : ISpell
    {
        public void CastBasicSpell()
        {
            Console.WriteLine("Fireball deals 100dmg");
        }
    }
}
