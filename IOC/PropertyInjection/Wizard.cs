﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyInjection
{
    public class Wizard
    {

        //podawanie klasy spel przez to property
        //czyli wstrzykiwanie 
        [Inject]
        public ISpell _spell { get; set; }
            
        public void CastBasicSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
