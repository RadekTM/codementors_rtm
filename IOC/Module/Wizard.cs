﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    public class Wizard
    {
        private IFireSpell _firespell;
        private IIceSpell _icespell;

        public Wizard (IFireSpell firespell, IIceSpell icespell)
        {
            _firespell = firespell;
            _icespell = icespell;
        }

        public void CastFireSpell()
        {
            _firespell.CastBasicSpell();
        }

        public void CastIceSpell()
        {
            _icespell.CastBasicSpell();
        }

    }
}
