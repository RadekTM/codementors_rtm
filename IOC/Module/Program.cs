﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new SpellModule());

            var wizard = kernel.Get<Wizard>();

            wizard.CastFireSpell();
            wizard.CastIceSpell();
        }
    }
}
