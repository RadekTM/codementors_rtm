﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    public class SpellModule:NinjectModule

    {
        public override void Load()
        {
            Bind<IFireSpell>().To<FireSpell>();
            Bind<IIceSpell>().To<IceSpell>();
        }
    }
}
