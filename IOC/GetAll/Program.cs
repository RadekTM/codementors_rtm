﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAll
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel container = new StandardKernel();

            container.Bind<ISpell>().To<FireSpell>();
            container.Bind<ISpell>().To<IceSpell>();

            var wizard = container.Get<Wizard>();

            wizard.CastBasicSpell();

        }
    }
}
