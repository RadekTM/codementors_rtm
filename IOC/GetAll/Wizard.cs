﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAll
{
    public class Wizard
    {

        public ISpell[] _spells;

       
        public Wizard(ISpell[] spells)
        {
            _spells= spells;
        }

        //działa też z listą
        /*
          public List<Ispell> _spells;

       
        public Wizard(List<ISpell> spells)
        {
            _spells= spells;
        }
         */

        public void CastBasicSpell()
        {
            foreach (var spell in _spells)
            {
                spell.CastBasicSpell();
            }
        }
    }
}
