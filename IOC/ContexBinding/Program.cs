﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContexBinding
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();

            //wstrzykujemy firespell na ispell ale użuj tego gdy wsztrykujemy to do jklasy fire wizard
            kernel.Bind<ISpell>().To<FireSpell>().WhenInjectedInto<FireWizard>();
            //wstrzykujemy icespell na ispell ale użuj tego gdy wsztrykujemy to do jklasy icewizard
            kernel.Bind<ISpell>().To<IceSpell>().WhenInjectedInto<IceWizard>();

            var firewizard = kernel.Get<FireWizard>();
            var icewizard = kernel.Get<IceWizard>();

            firewizard.CastBasicFireSpell();
            icewizard.CastBasicIceSpell();

        }
    }
}
