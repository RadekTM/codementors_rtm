﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContexBinding
{
    public class IceWizard
    {
        private ISpell _spell;

        public IceWizard(ISpell spell)
        {
            _spell = spell;
        }

        public void CastBasicIceSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
