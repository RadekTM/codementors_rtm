﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContexBinding
{
    public class FireWizard
    {

        private ISpell _spell;

        public FireWizard(ISpell spell)
        {
            _spell = spell;
        }

        public void CastBasicFireSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
