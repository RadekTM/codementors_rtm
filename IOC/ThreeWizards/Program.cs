﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    class Program
    {
        static void Main(string[] args)
        {

            IKernel kernel = new StandardKernel(new SpellModule(), new MagicModule());

            var firewizard = kernel.Get<FireWizard>();
            var waterwizard = kernel.Get<WaterWizard>();
            var icewizard = kernel.Get<IceWizard>();

            firewizard.CastBasicSpell();
            firewizard.UseMagicBall();

            waterwizard.CastBasicSpell();
            waterwizard.UseMagicBall();

            icewizard.CastBasicSpell();
            waterwizard.UseMagicBall();
        }
    }
}
