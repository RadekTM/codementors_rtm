﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    public abstract class Wizards
    {
        protected ISpell _spell;
        protected IMagicBall _magicball;

        public void UseMagicBall()
        {
            _magicball.CastMagicBall();
        }

        public void CastBasicSpell()
        {
            _spell.CastBasicSpell();

        }

    }
}
