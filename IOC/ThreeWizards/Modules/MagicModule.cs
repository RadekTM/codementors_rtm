﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
   public class MagicModule:NinjectModule
    {
         public override void Load()
            {

            Bind<IMana>().To<Mana>();
            Bind<IMagicBall>().To<MagicBall>();
            }
    }
}
