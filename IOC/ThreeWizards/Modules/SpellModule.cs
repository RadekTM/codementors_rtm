﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    public class SpellModule:NinjectModule

    {
        public override void Load()
        {
            Bind<ISpell>().To<FireSpell>().WhenInjectedInto<FireWizard>();
            Bind<ISpell>().To<IceSpell>().WhenInjectedInto<IceWizard>();
            Bind<ISpell>().To<WaterSpell>().WhenInjectedInto<WaterWizard>();
        }
    }
}
