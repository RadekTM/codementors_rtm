﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    public class FireSpell : ISpell
    {
        private IMana _mana;

        //ten konstruktor wstrzykuje imana
        public FireSpell(IMana mana)
        {
            _mana = mana;
        }

        public void CastBasicSpell()
        {
            Console.WriteLine("Fireball deals 100dmg");
            _mana.CastBasicMana();
        }
    }
}
