﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    public class WaterSpell:ISpell
    {
        private IMana _mana;

        //ten konstruktor wstrzykuje imana
        public WaterSpell(IMana mana)
        {
            _mana = mana;
        }

        public void CastBasicSpell()
        {
            Console.WriteLine("Waterfall deals 1000dmg");
            _mana.CastBasicMana();

        }
    }
}
