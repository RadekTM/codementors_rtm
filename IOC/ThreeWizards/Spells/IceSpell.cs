﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeWizards
{
    public class IceSpell : ISpell
    {
        private IMana _mana;

        //ten konstruktor wstrzykuje imana
        public IceSpell(IMana mana)
        {
            _mana = mana;
        }

        public void CastBasicSpell()
        {
            Console.WriteLine("IceBall deals 40dmg");
            _mana.CastBasicMana();

        }
    }
}
