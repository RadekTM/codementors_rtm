﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public class Wizard
    {
        private ISpell _spell;


        //to wstrzykuje nam obiekt klasy ISpell
        //wsztrykiwanie przez kostruktor
        //kontener szuka w którym jaest najwiecej paremetrów któe jest w stanie w zalezności rozwiązać

        //albo możemy sami ozanaczyć o który konstruktor chodzi
       // [Inject]
        public Wizard(ISpell spell)
        {
            _spell = spell;
        }

       
        public void CastBasicSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
