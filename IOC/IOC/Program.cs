﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    class Program
    {
        static void Main(string[] args)
        {
            //kernel to kontener IOC w nim rejestrujemy zaleznośći
            IKernel kernel = new StandardKernel();
            

            //tu ustawiamy zależności
            kernel.Bind<ISpell>().To<Spell>();
            //rejestrowanie nowej zależności ale mimo iż wywołana jest w spell to wskazujemy że imana woła mana
            kernel.Bind<IMana>().To<Mana>();

            //tworzymy obiekt
            var wizard = kernel.Get<Wizard>();

            //wywołujemy metode na obiekcie
            wizard.CastBasicSpell();
        }
    }
}
