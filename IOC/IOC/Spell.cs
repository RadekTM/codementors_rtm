﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public class Spell : ISpell
    {
        private IMana _mana;

        //ten konstruktor wstrzykuje imana
        public Spell(IMana mana)
        {
            _mana = mana;
        }

        public void CastBasicSpell()
        {
            Console.WriteLine("Fireball deals 100dmg");
            _mana.CastBasicMana();
        }
    }
}
