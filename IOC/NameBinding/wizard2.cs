﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameBinding
{
    public class Wizard2
    {
        public ISpell _spell;

        //nazwa konstruktora który jest później wiązany w kontenerze
   
        [Inject]
        public Wizard2([Named("fire")]ISpell spell)
        {
            _spell = spell;
        }


        public Wizard2()
        {

        }

        public void CastFireSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}

