﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameBinding
{
    class Program
    {
        static void Main(string[] args)
        {
            //kernel to kontener IOC w nim rejestrujemy zaleznośći
            //tu tworzymu instancje kontenera (nazwa kernel)
            //który trzyma zalezności
            IKernel kernel = new StandardKernel();


            //tu mówimy kontenerowi o instancje jakiej klasy nam chodzi
            kernel.Bind<ISpell>().To<FireSpell>().Named("fire");
            kernel.Bind<ISpell>().To<IceSpell>().Named("ice");

            //prosimy o tworzenie instancji klasy wizard i wizard2
            var wizard = kernel.Get<Wizard>();
            var fireWizard = kernel.Get<Wizard2>();
            //wywołujemy metode na obiektach
            wizard.CastBasicSpell();
            fireWizard.CastFireSpell();
        }
    }
}
