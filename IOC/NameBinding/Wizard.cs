﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameBinding
{
    public class Wizard
    {

        public ISpell _spell;

        [Inject]
        public Wizard([Named("ice")]ISpell spell)
        {
            _spell = spell;
        }


        public Wizard()
        {

        }

        public void CastBasicSpell()
        {
            _spell.CastBasicSpell();
        }
    }
}
