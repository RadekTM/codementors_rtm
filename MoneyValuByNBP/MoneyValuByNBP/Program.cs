﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MoneyValuByNBP
{
    public class Program
    {
        public class Money
        {
            public string Waluta { get; set; }
            public int KursSredni { get; set; }
            public string KodWaluty { get; set; }
        }

        public void KursWalut()
        {
            Money money = new Money();
            List<Money> moneyList = new List<Money>();

            XElement xml = XElement.Load("http://www.nbp.pl/kursy/xml/LastA.xml");
            money.Waluta = xml.Element("Imie").Value;
            money.KursSredni = Int32.Parse(xml.Element("Wiek").Value);
            money.KodWaluty = xml.Element("Wlasciciel").Value;
            moneyList.Add(money);
            /* XmlSerializer xmlSerializer = new XmlSerializer(typeof(Kot), new XmlRootAttribute("Kot"));

            StringBuilder stringBuilder = new StringBuilder();
            xmlSerializer.Serialize(new StringWriter(stringBuilder), kot);

            Console.WriteLine(stringBuilder);*/
        }

        static void Main(string[] args)
        {

            XmlDocument oXmlDocument1 = new XmlDocument();
            oXmlDocument1.Load("http://www.nbp.pl/kursy/xml/LastA.xml");
            XmlDocument oXmlDocument2 = new XmlDocument();
            oXmlDocument2.Load("http://www.nbp.pl/kursy/xml/LastB.xml");
            XmlDocument oXmlDocument = oXmlDocument1;
            XmlNodeList oPersonNodesList = oXmlDocument.GetElementsByTagName("pozycja");
            XmlNodeList date = oXmlDocument.GetElementsByTagName("data_publikacji");
            foreach(XmlNode item in date)
            {
                Console.WriteLine(item.FirstChild.InnerText);
            }
            Console.WriteLine("W pliku XML, znaleziono dane następujących walut: ");
            foreach (XmlNode oPerson in oPersonNodesList)
            {
                Console.WriteLine("nazwa_waluty: {0}, przelicznik: {1}, kurs_sredni : {3}, kod_waluty {2}",
                    oPerson.FirstChild.InnerText,
                    oPerson.FirstChild.NextSibling.InnerText,
                    oPerson.LastChild.PreviousSibling.InnerText,
                    oPerson.LastChild.InnerText);
            }
            Console.WriteLine("Proszę o wybranie waluty");
            Console.WriteLine("1 - EURO");
            Console.WriteLine("2 - USD");
            Console.WriteLine("3 - GBP");
            var selection = Console.ReadKey().KeyChar;
            string valuta = "PLN";
            switch (selection)
            {
                case '1':
                {
                        valuta = "EUR";
                        break;

                }
                case '2':
                    {
                        valuta = "USD";
                        break;

                    }
                case '3':
                    {
                        valuta = "GBP";
                        break;

                    }
                default:
                    {
                        Console.WriteLine("brak danych");
                        break;

                    }

            }

            Console.WriteLine($"{valuta}");
            foreach (XmlNode item in oPersonNodesList)
            {
                
                if(item.LastChild.PreviousSibling.InnerText == valuta)
                {
                    Console.WriteLine($"Kurs wynosi:{item.LastChild.InnerText}");
                    Console.Write("Podaj kwote do przeliczenia: ");
                    decimal curency = Convert.ToDecimal(item.LastChild.InnerText);
                    uint amount = Convert.ToUInt32(Console.ReadLine());
                    decimal money = amount * curency;
                    Console.WriteLine($"Wartość transkacji po kursie średnim, to {money:#.00}PLN");
                    Console.ReadKey();

                }
            }
            
            
            Console.ReadKey();
        }
    }
}
