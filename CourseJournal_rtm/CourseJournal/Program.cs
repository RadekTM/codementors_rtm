﻿using ConsoleTools.Menu;
using CourseJournal.BL;
using CourseJournal.BL.Model;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CourseJournal
{
    class Program
    {
        static Journal journal = new Journal();
        static DataCheck checker = new DataCheck();
        static StringChecker stringChecker = new StringChecker();
        static Teacher teacher1 = new Teacher();


        public static void Main(string[] args)
        {
              var exitOption = new ExitOption();
                var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                    new GeneralOption("Utwórz kurs",AddCourse),
                    new GeneralOption("Dodaj pracę domową",AddHomework),
                    new GeneralOption("Dodaj listę obecności",AddPresence),
                    new GeneralOption("Kartoteka kursu",KontrolaDanych),
                    exitOption
                });

                do
                {
                    Console.Clear();
                    consoleMenu.Show();
                } while (!exitOption.IsExitRequested);
            Console.ReadLine();
        }

        private static void AddPresence()
        {
            Console.Clear();
            if (journal.StudentList.Count == 0)
            {
                Console.Write($"\nProszę stworzyć najpierw liste ");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine($"Ocena pracy domowej uczestników kursu {journal.CourseName}");
                Console.Write($"\nProszę podać ilość dni trwania kursu:  ");
                uint dlugoscKursu = Convert.ToUInt32(Console.ReadLine());
                               
                foreach (var student in journal.StudentList)
                {
                    uint obecnosc =0, counter =0;
                    Console.Write($"\nProszę podać czy student/ka: {student.Name} {student.Surname}: ");
                    for (int i = 1; i<= dlugoscKursu;i++)
                    {
                        Console.WriteLine($"Był obecny dnia {i}, 0-nieobcny 1-obecny");
                        counter = Convert.ToUInt32(Console.ReadLine());
                        if (counter > 1) counter = 1;
                        obecnosc += counter;
                     }

                    decimal presenceCounter = (((decimal)obecnosc / dlugoscKursu)*100);
                    journal.studentPrsence(student, presenceCounter);
                }

            }
        }

        /*private static void AddPresence()
        {
            Console.Clear();
            if (journal.StudentList.Count == 0)
            {
                Console.Write($"\nProszę stworzyć najpierw liste ");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine($"Ocena pracy domowej uczestników kursu {journal.CourseName}");
                Console.Write($"\nProszę podać ilość dni trwania kursu:  ");
                int dlugoscKursu = Convert.ToInt32(Console.ReadLine());

                foreach (var student in journal.StudentList)
                {
                    Console.Write($"\nProszę podać ilość dni w jakich był/ła obecny/a: {student.Name} {student.Surname}: ");
                    int obecnosc = Convert.ToInt32(Console.ReadLine());
                    decimal presenceCounter = (((decimal)obecnosc / dlugoscKursu) * 100);
                    journal.studentPrsence(student, presenceCounter);
                }

            }
        }*/

        private static void AddHomework()
        {
            Console.Clear();
            if (journal.StudentList.Count == 0)
            {
                Console.Write($"\nProszę stworzyć najpierw liste ");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine($"Ocena pracy domowej uczestników kursu {journal.CourseName}" );
                Console.Write($"\nProszę podać maksymalną ilość punktów z pracy domowej: ");
                int MaxHomeworkScore = Convert.ToInt32(Console.ReadLine());

                foreach (var student in journal.StudentList)
                {
                    Console.Write($"\nProszę podać ilość otrzymanych punktów z pracy domowej dla: {student.Name} {student.Surname}: ");
                    int personalScore = Convert.ToInt32(Console.ReadLine());
                    decimal homeworkCounter = (((decimal)personalScore / MaxHomeworkScore)*100);
                    journal.studentHomework(student, homeworkCounter);
                }

            }
        }


        private static void AddCourse()
        {
            Console.Clear();
            Console.WriteLine("Tworzenie dziennike kursu");
            Console.WriteLine("");
            journal.CourseName = stringChecker.NullString($"\nProszę podać nazwę kursu: ");
            teacher1.Name = stringChecker.NullString($"\nProszę podać Imię prowadzącego: ");
            teacher1.Surname = stringChecker.NullString($"\nProszę podać Nazwisko prowadzącego: ");
            journal.teacher = teacher1;
            Console.Write($"\nData rozpoczęcia kursu. ");
            journal.CourseDate = checker.DataChecker();
            journal.pHomework = Convert.ToInt32(
                stringChecker.NullString($"\nProszę podać próg zaliczenia prac domowych (procent): "));
            journal.pPresence = Convert.ToInt32(
                stringChecker.NullString($"\nProszę podać próg zaliczenia obecności (procent): "));
            journal.PupilsNumber = Convert.ToInt32(
                stringChecker.NullString($"\nProszę podać ilość kursantów: "));
            for (int i = 1; i <= journal.PupilsNumber; i++)
            {
                string Name = stringChecker.NullString($"Proszę imie studenta {i}: ");
                string SurrName = stringChecker.NullString($"\nProszę Nazwisko studenta {i}: ");
                uint Id = Convert.ToUInt32(stringChecker.NullString($"\nProszę podać id studenta {i}: "));
                Console.Write($"\nData urodzenia studenta {i}.");
                DateTime BirthDate = checker.DataChecker();
                char gender='a';
                bool genderMenu = true;
                do {
                    Console.Write($"\nProszę Płeć studenta {i} (M)ale/(F)male: ");
                    char typed = Console.ReadKey().KeyChar;
                    if (typed == 'F' || typed == 'f')
                    {
                        gender = typed;
                        genderMenu = false;
                    }
                    else if (typed == 'M' || typed == 'm')
                    {
                        gender = typed;
                        genderMenu = false;
                    }
                    else
                    {
                        genderMenu = true;
                    }

                } while (genderMenu == true);
                journal.AddStudent(new Student(Name, SurrName, Id, BirthDate, gender));
                Console.WriteLine(" ");
            }
            Thread.Sleep(200);
        }

        private static void KontrolaDanych()
        {
            if(journal.StudentList.Count == 0)
            {
                Console.WriteLine("Brak pełnych danyk kursu - proszę o wprowadzenie lub uzupełnienie danych");
            }
            else {
                journal.CourseInfo();
                Console.WriteLine("\nStudenci uczestniczący w zajęciach:");
                journal.daneStudenta();
                Console.WriteLine("\nStudent score:");
                Console.WriteLine(" ");
                journal.StudenatAtendance();
                Console.WriteLine(" ");
                journal.StudenatScore();
                Console.WriteLine("\nAby powrócić do głownego menu naciśnij dowolny klawisz....");
                Console.ReadKey();
            }
        }
    }
}
