﻿using CourseJournal.BL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseJournal.BL
{
    public class Journal
    {
        public string CourseName { get; set; }
        public DateTime CourseDate { get; set; }
        public int pHomework { get; set; }
        public int pPresence { get; set; }
        public int PupilsNumber { get; set; }
        public Teacher teacher { get; set; }
        //public Student student { get; set; }
        public List<Student> StudentList { get; set; }
        public decimal presenceCounter { get; set; }
        public decimal homeworkCounter { get; set; }

        Dictionary<Student, decimal> _studentPrsence { get; set; }
            = new Dictionary<Student, decimal>();
        Dictionary<Student, decimal> _studentHomework { get; set; }
            = new Dictionary<Student, decimal>();

        public void studentPrsence(Student student, decimal score)
        {
            _studentPrsence.Add(student, score);
        }

        public void StudenatAtendance()
        {
            foreach (KeyValuePair<Student, decimal> pair in _studentPrsence)
            {
                Console.WriteLine($"Student {pair.Key.Name}, {pair.Key.Surname}");
                Console.WriteLine($"był obecny :{pair.Value:N2} procent czasu trwania kursu");
                if (pair.Value >= pPresence)
                {
                    Console.ForegroundColor = ConsoleColor.Green; 
                    Console.WriteLine("Frekfencja - zaliczona"); 
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Frekfencja - niezaliczona");
                    Console.ResetColor();
                }
            }
        }

        public void studentHomework(Student student, decimal score)
        {
            _studentHomework.Add(student, score);
        }

        public void StudenatScore()
        {
            foreach (KeyValuePair<Student, decimal> pair in _studentHomework)
            {
                Console.WriteLine($"Student {pair.Key.Name} {pair.Key.Surname}");
                Console.WriteLine($"zdobył :{pair.Value:N2} procent z prac domych");
                if (pair.Value >= pPresence)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Prace domowe - zaliczone");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Prace domowe - niezaliczona");
                    Console.ResetColor();
                }
            }
        }

        public void CourseInfo()
        {
            Console.WriteLine($"Nazwa kursu:  {CourseName}, data rozpoczęcia: {CourseDate.Year}" +
                $"-{CourseDate.Month}-{CourseDate.Day}");
            Console.WriteLine($"Prowadzący: {teacher.Name} {teacher.Surname}");
            Console.WriteLine($"Próg zaliczenia prac domowych: {pHomework}%");
            Console.WriteLine($"Próg zaliczenia obecności: {pPresence}%");
        }

        //stworzenie pustej listy

        
        public Journal()
        {
            StudentList = new List<Student>();
        }


        //dodawanie studenta do listy
        public void AddStudent(Student student)
        {
            StudentList.Add(student);
        }

        public void daneStudenta()
        {
            int i = 1;
            foreach (Student pupils in StudentList)
            {
                Console.WriteLine($"Student {i}: {pupils.Surname} {pupils.Name}");
                Console.WriteLine($"Student {i} - ID:{pupils.Id}, " +
                    $"data urodzenia:{pupils.BirthDate}, płeć: {pupils.Gender}");
                i++;
            }
        }



    }
}
