﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseJournal.BL
{
    public class StringChecker
    {
        public string NullString(string InputText)
        {
            string tempString;
            do
            {
                Console.Write($"{InputText}");
                tempString = Console.ReadLine();
                if (string.IsNullOrEmpty(tempString))
                {
                    Console.Write($"{InputText}");
                    tempString = Convert.ToString(Console.ReadLine());
                }
            } while (string.IsNullOrEmpty(tempString) == true);
            return tempString;
        }
    }
}
