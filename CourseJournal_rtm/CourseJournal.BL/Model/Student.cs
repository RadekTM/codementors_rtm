﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseJournal.BL.Model
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public uint Id { get; set; }
        public DateTime BirthDate { get; set; }
        public char Gender { get; set; }

        public Student(string SName, string SSurname, uint Sid, DateTime SBirthdate, char Sgender)
        //public Student(string SName, string SSurname)
        {
            Name = SName;
            Surname = SSurname;
            Id = Sid;
            BirthDate = SBirthdate;
            Gender = Sgender;
        }

    }
}
            /*
            

        }



        public override string ToString()
        {
            return Name + Surname + Id + BirthDate + Gender;
        }
    }
}*/
