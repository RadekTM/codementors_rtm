﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Homework1
{
    internal class Library
    {
        private List<Books> BookList;

        public Library()
        {
            BookList = new List<Books>();
        }

        public void AddBook(Books newBook)
        {
            BookList.Add(newBook);
        }

        public void SearchingByAuthor()
        {
            Console.Clear();
            Console.WriteLine("Wyszykiwanie po nazwisku Autora.");
            Console.Write("\nProszę podać nazwisko autora: ");
            string AuthorName = Console.ReadLine();
            int i = 1;
            int w;
            bool check = true;  // zmienna pomocnicza;
            foreach (Books aname in BookList)
            {
                if (aname.AuthorSureName.ToLower() == AuthorName.ToLower())
                {
                    check = false;
                    Console.WriteLine("\nLp. " + i + " " + aname.BookTitle + " - " + aname.AuthorName + " " + aname.AuthorSureName
                        + "  (" + aname.PublicationYear + "r.)");
                    i++;
                    if (aname.InCollection == true)
                    {
                        Console.WriteLine("Książka należy do kolekcji/serii: " + aname.BookCollectionTitle);
                    }
                }
            }
            if (check == true)  
            {
                Console.WriteLine("Nie znaleziony książki o takim autorze");
            }
        }

        public void SearchingByTitle()
        {
            Console.Clear();
            Console.WriteLine("Wyszykiwanie po tytule książki.");
            Console.Write("Proszę podać tytuł książki: ");
            string BookTitle = Console.ReadLine();
            int i = 1;
            bool check = true;
            foreach (Books bname in BookList)
            {
                if (bname.BookTitle.ToLower() == BookTitle.ToLower())
                {
                    check = false;   
                    Console.WriteLine("\nLp. " + i + " " + bname.BookTitle + " - " + bname.AuthorName + " "
                        + bname.AuthorSureName + "(" + bname.PublicationYear + ".r)");
                    i++;
                    if (bname.InCollection == true)
                    {
                        Console.WriteLine("Książka należy do kolekcji/serii: " + bname.BookCollectionTitle);
                        Console.WriteLine("\nPozostałe książki należące do tej samej kolekcji/serii:");
                    }
                    foreach (Books temp in BookList)
                    {
                        if (bname.InCollection == true)
                        {
                            if (temp.BookCollectionTitle == bname.BookCollectionTitle)
                            {
                                if (temp.BookTitle != bname.BookTitle)
                                {
                                    Console.WriteLine(temp.BookTitle + " - " + temp.AuthorName + "  " + temp.AuthorSureName
                                        + " (" + temp.PublicationYear + ".r)");
                                }
                            }
                        }
                    }
                }
            }

            if (check == true)
            {
                Console.WriteLine("Nie znaleziony książki o podanym tytule");
            }
        }
    }
}

