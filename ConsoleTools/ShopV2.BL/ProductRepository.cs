﻿using ShopV2.BL.Model;
using ShopV2.BL.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopV2.BL
{
    public class ProductRepository
    {
        private readonly Dictionary<string, Product> _products
            = new Dictionary<string, Product>();

        public void AddProduct(Product product)
        {
            using (var ctx = new ShopContext())
            {
                ctx.Products.Add(new ProductDto(product));

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Finds a product by name
        /// </summary>
        /// <param name="name">name to be found</param>
        /// <returns>found product or null if not found</returns>
        public Product GetProduct(string name)
        {
            List<ProductDto> p;
            using (var ctx = new ShopContext())
            {
                p= ctx.Products.ToList();
            }
            foreach (var product in p)
            {
                if (product.Name == name)
                {
                    return product.ToProduct();
                }
            }
            return null;

        }
    }
}
